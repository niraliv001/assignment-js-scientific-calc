console.log("hello");
let entered = document.getElementById("calculate_it");

function trigno(fun) {
  let enteredValue = entered.innerText;
  let result;
  switch (fun) {
    case "sine":
      result = Math.sin(enteredValue);
      break;
    case "cos":
      result = Math.cos(enteredValue);
      break;
    case "tan":
      result = Math.tan(enteredValue);
      break;
    case "cot":
      result = 1 / Math.tan(enteredValue);
      break;
    case "cosec":
      result = 1 / Math.sin(enteredValue);
      break;
    case "sec":
      result = 1 / Math.cos(enteredValue);
      break;
  }
  if (result === "NaN") {
    entered.innerText = "Error";
  } else {
    entered.innerText = result;
  }
}

function func(fun) {
  let enteredValue = entered.innerText;
  let result;
  switch (fun) {
    case "random":
      result = Math.random() * 100;
      break;
    case "ceil":
      result = Math.ceil(enteredValue);
      break;
    case "floor":
      result = Math.floor(enteredValue);
      break;
    case "round":
      result = Math.round(enteredValue);
      break;
  }
  if (result === "NaN") {
    entered.innerText = "Error";
  } else {
    entered.innerText = result;
  }
}

function addChar(element) {
  let char = element.getAttribute("data-value");
  if (entered.innerText == 0) {
    entered.innerText = "";
  }
  if (char) {
    if (entered.innerText == "3.14") {
      entered.innerText ="3.14*";
    }
  entered.innerText += char;
  }
}
function F_E() {
  ex = Number(entered.innerText);

  entered.innerText = ex.toExponential();
}
function DEG() {
  let deg = entered.innerText;
  let ans = (Math.PI * deg) / 180;
  entered.innerText = ans.toPrecision(6);
}
function cube() {
  let enteredValue = entered.innerText;
  entered.innerText = Math.pow(enteredValue, 3);
}
function clearScreen() {
  entered.innerText = "0";
}

function backspace() {
  let enteredValue = entered.innerText;
  let newenteredValue = enteredValue.slice(0, enteredValue.length - 1);
  entered.innerText = newenteredValue;
}

function calculate() {
  let enteredValue = entered.innerText;
  entered.innerText = eval(enteredValue);
}
function square() {
  let enteredValue = entered.innerText;
  entered.innerText = Math.pow(enteredValue, 2);
}

function absolute() {
  entered.innerText = Math.abs(entered.innerText);
}

function xp() {
  entered.innerText = Math.exp(entered.innerText).toPrecision(6);
}

function sqrt() {
  entered.innerText = Math.sqrt(entered.innerText);
}

function factorial() {
  let enteredValue = entered.innerText;
  let result = 1;
  for (i = enteredValue; i >= 1; i--) {
    result *= i;
  }
  entered.innerText = result;
}

function ten_pow() {
  entered.innerText = Math.pow(10, entered.innerText);
}

function log() {
  entered.innerText = Math.log10(entered.innerText);
}

function ln() {
  entered.innerText = Math.log(entered.innerText);
}
//for memory
let memoryArr = [];
let mStatus = 0;

function Clear() {
  memoryArr = [];
  if (mStatus == 1) {
    document.getElementById("memory-clear").style.opacity = 0.2;
    document.getElementById("memory-recall").style.opacity = 0.2;
  }
}

function Recall() {
  let sum = 0;
  for (let x of memoryArr) {
    sum += Number(x);
  }
  entered.innerText = sum;
}

function Plus() {
  if (entered.innerText != "") {
    memoryArr.push(entered.innerText);
  }
  if (mStatus == 0 && entered.innerText != "") {
    document.getElementById("memory-clear").style.opacity = 1;
    document.getElementById("memory-recall").style.opacity = 1;
    mStatus = 1;
  }
  entered.innerText = "";
}

function Minus() {
  if (entered.innerText != "") {
    memoryArr.push("-" + entered.innerText);
  }
  if (mStatus == 0 && entered.innerText != "") {
    document.getElementById("memory-clear").style.opacity = 1;
    document.getElementById("memory-recall").style.opacity = 1;
    mStatus = 1;
  }
  entered.innerText = "";
}

function Save() {
  if (entered.innerText != "") {
    memoryArr.push(entered.innerText);
  }
  if (mStatus == 0 && entered.innerText != "") {
    document.getElementById("memory-clear").style.opacity = 1;
    document.getElementById("memory-recall").style.opacity = 1;
    mStatus = 1;
  }
  entered.innerText = "";
}
function x_y() {
  console.log(entered.innerHTML);
}
